import multiprocessing
import requests
from bs4 import BeautifulSoup
from time import sleep


def controller():
    base = 'https://www.distrowatch.com'
    queue = multiprocessing.Queue()
    results = multiprocessing.Queue()
    queue.put(base)

    procs = []
    for x in range(2):
        procs.append(multiprocessing.Process(target=pagescraper, args=(base, queue, results)))
        procs[-1].start()

    counter = 0
    while True:
        try:
            results.get_nowait()
            print(results)
        except:
            sleep(5)
            counter+=1
            if counter > 15:
                break

def pagescraper(base, queue, results):
    session = requests.Session()
    counter = 0
    while True:
        rec = queue.get()
        resp = session.get(rec)
        try:
            if resp.status_code in [200, 301]:
                soup = BeautifulSoup(resp.content, features="lxml")
                for link in soup.find_all('a', href=True):
                    if 'http' not in link['href']:
                        new_url =f"{base}/{link['href']}"
                        print(new_url)
                        queue.put(new_url)
                    results.put(resp.content)
            else:
                queue.put(rec)
        except Exception as e:
            print(e)
            counter += 1
            queue.put(rec)
            if counter > 4:
                break


